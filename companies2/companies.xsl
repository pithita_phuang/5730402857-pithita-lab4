<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/companies">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title> 
            </head>
            <body>
                <h2> These are the leading companies:</h2>  
                <xsl:for-each select="company"> 
                  <ul>
                    <li>
                         <xsl:value-of select="name"/>
                            <xsl:for-each select="research">
                              <xsl:for-each select="labs/lab">
                               <ul>
                               <li>
                                    <xsl:value-of select="."/>                                  
                               </li>
                               </ul>
                              </xsl:for-each>
                            </xsl:for-each>
                        
                    </li>
                  </ul>
                 </xsl:for-each>               
                
            </body>
        </html>
</xsl:template>
</xsl:stylesheet>
