<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:kml ="http://www.opengis.net/kml/2.2"
                xmlns:atom="http://www.w3.org/2005/Atom" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <img style = "float:left;">
                    <xsl:attribute name="src">
                        <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        
                    </xsl:attribute>
                </img>
                <h1>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                List of hotels
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                        <xsl:sort lang="kml:name"/>
                        <li>
                            
                            <xsl:value-of select="kml:name"/>
                        </li>
                        <ul>
                            <li>
                                <a>
                                    <xsl:attribute name="href">
                                       <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/>   
                                    </xsl:attribute>
                                       <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/>
                                </a>
                            </li>
                            <li>    
                                Coordinate: <xsl:value-of select="kml:Point/kml:coordinates"/>       
                            </li>
                        </ul> 
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
