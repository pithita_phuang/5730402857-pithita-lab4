<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf=  "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library" >
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <table border = "1">
                    <th	align="left">Title</th>
                    <th align="left">Pages</th>
                    <tr>
                        <xsl:for-each select="rdf:RDF/rdf:Description">
                            <xsl:sort ascending ="lib:pages"/>
                                <xsl:if test="lib:pages">
                                    <tr>
                                        <td>
                                            <xsl:value-of select="@about"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="lib:pages"/>
                                        </td>
                                    </tr>
                                </xsl:if>
                        </xsl:for-each>
                    </tr>
                  
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
