<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/companies">
 <html>
    <head>
     <title>XSLT Lab : Problem 1.1</title>
    </head>
     <body>
        <ul>
         <xsl:for-each select="company"> 
             <li>
                 <xsl:value-of select="name"/>
             </li>
         </xsl:for-each>    
        </ul>        
     </body>   
</html>
</xsl:template>
</xsl:stylesheet>
