<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8"/>
    <xsl:template match="/companies">
        <xsl:element name="companies">
            <xsl:for-each select="company">
                <xsl:element name="company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>
                
                <xsl:element name="name">
                    <xsl:value-of select="name"/>
                </xsl:element>
                <xsl:for-each select="research/labs">
                    <xsl:for-each select="lab">
                        <xsl:value-of select="lab"/>
                        <xsl:element name="lab">
                            <xsl:attribute name="state">
                                <xsl:choose>
                                    <xsl:when test ="@location='new york city'">New York</xsl:when>
                                    <xsl:when test ="@location='san jose'">California</xsl:when>
                                    <xsl:when test ="@location='pittsburgh'">Pennsylvania</xsl:when>
                                
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:for-each>
                </xsl:element>
            </xsl:for-each>

              
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>